import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

import "./styles.scss";
// import { Container } from './styles';

// página sobre a aplicação e criador

const About: React.FC = () => {
  return (
    <>
      <div className="bg-underlay">
        <Navbar bg="light" expand="lg">
          <div className="home-container">
            <img src={require("../../assets/images/logo.png")} alt="logo" />
            <Navbar.Brand href="/">JampaFinder</Navbar.Brand>
          </div>
          <Navbar.Collapse>
            <Link to="/">
              <Button variant="primary">Início</Button>
            </Link>
          </Navbar.Collapse>
        </Navbar>
      </div>
      <div className="mid-about">
        <h1>Somos uma plataforma de comunicação da cidade de João Pessoa</h1>
        <p>
          Nossa plataforma serve para usuários de João Pessoa conseguirem dar
          feedbacks sobre seus bairros para que assim aja uma melhor
          comunicação, e cobranças de melhoras. Logue, escolha o bairro e mande
          sua mensagem. De qualquer lugar, para seu lugar. <br />
          Nossa plataforma serve para usuários de João Pessoa conseguirem dar
          feedbacks sobre seus bairros para que assim aja uma melhor
          comunicação, e cobranças de melhoras. Logue, escolha o bairro e mande
          sua mensagem. De qualquer lugar, para seu lugar.
          {/* Nossa plataforma serve para
          usuários de João Pessoa conseguirem dar feedbacks sobre seus bairros
          para que assim aja uma melhor comunicação, e cobranças de melhoras.
          Nossa plataforma serve para usuários de João Pessoa conseguirem dar
          feedbacks sobre seus bairros para que assim aja uma melhor
          comunicação, e cobranças de melhoras. */}
        </p>
      </div>
      <div className="bottom">
        <a href="https://github.com/matheuscorreiag">
          <img src={require("../../assets/images/github-logo.svg")} alt="git" />
        </a>
        <a href="https://instagram.com/matheuscorreiag">
          <img
            src={require("../../assets/images/instagram-seeklogo.com.svg")}
            alt="instagram"
          />
        </a>
      </div>
    </>
  );
};

export default About;
